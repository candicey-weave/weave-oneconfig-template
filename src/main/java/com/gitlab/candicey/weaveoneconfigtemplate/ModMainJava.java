package com.gitlab.candicey.weaveoneconfigtemplate;

import com.gitlab.candicey.zenithloader.ZenithLoader;
import com.gitlab.candicey.zenithloader.dependency.Dependencies;
import net.weavemc.loader.api.ModInitializer;
import net.weavemc.loader.api.event.EventBus;
import net.weavemc.loader.api.event.StartGameEvent;

public class ModMainJava implements ModInitializer {
    private static YourConfigJava configJava;

    @Override
    public void preInit() {
        System.out.println("[WeaveOneConfigTemplate] Initialising...");

        ZenithLoader.INSTANCE.loadDependencies(
                Dependencies.INSTANCE.getConcentra().invoke(
                        // weaveoneconfigtemplate.versions.json
                        "weaveoneconfigtemplate"
                )
        );

        // We can't instantiate the config directly in preInit because OneConfig may not be loaded yet.
        EventBus.subscribe(StartGameEvent.Pre.class, (event) -> configJava = new YourConfigJava());

        System.out.println("[WeaveOneConfigTemplate] Initialised!");
    }

    public static YourConfigJava getConfigJava() {
        return configJava;
    }
}
