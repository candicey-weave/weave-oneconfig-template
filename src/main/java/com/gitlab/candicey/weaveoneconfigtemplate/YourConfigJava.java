package com.gitlab.candicey.weaveoneconfigtemplate;

import cc.polyfrost.oneconfig.config.Config;
import cc.polyfrost.oneconfig.config.annotations.Text;
import cc.polyfrost.oneconfig.config.data.Mod;
import cc.polyfrost.oneconfig.config.data.ModType;

public class YourConfigJava extends Config {
    @Text(name = "Some Text Box", description = "This is a text box")
    public String someTextBox = "default value";

    public YourConfigJava() {
        super(new Mod("Your Mod Name", ModType.PVP), "config-file-name.json");

        this.initialize();
    }
}
