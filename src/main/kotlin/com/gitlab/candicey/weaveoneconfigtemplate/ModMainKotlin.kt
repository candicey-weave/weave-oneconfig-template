package com.gitlab.candicey.weaveoneconfigtemplate

import com.gitlab.candicey.zenithloader.ZenithLoader
import com.gitlab.candicey.zenithloader.dependency.Dependencies.concentra
import net.weavemc.loader.api.ModInitializer
import net.weavemc.loader.api.event.EventBus
import net.weavemc.loader.api.event.StartGameEvent


class ModMainKotlin : ModInitializer {
    override fun preInit() {
        println("[WeaveOneConfigTemplate] Initialising...")

        ZenithLoader.loadDependencies(
            // weaveoneconfigtemplate.versions.json
            concentra("weaveoneconfigtemplate"),
        )

        // We can't instantiate the config directly in preInit because OneConfig may not be loaded yet.
        EventBus.subscribe(StartGameEvent.Pre::class.java) { YourConfigKotlin }

        println("[WeaveOneConfigTemplate] Initialised!")
    }
}