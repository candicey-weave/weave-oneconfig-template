package com.gitlab.candicey.weaveoneconfigtemplate

import cc.polyfrost.oneconfig.config.Config
import cc.polyfrost.oneconfig.config.annotations.Text
import cc.polyfrost.oneconfig.config.data.Mod
import cc.polyfrost.oneconfig.config.data.ModType

object YourConfigKotlin : Config(Mod("Your Mod Name", ModType.PVP), "config-file-name.json") {
    @Text(name = "Some Text Box", description = "This is a text box")
    var someTextBox: String = "default value"

    init {
        initialize()
    }
}